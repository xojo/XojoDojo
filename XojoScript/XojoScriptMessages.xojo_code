#tag Module
Protected Module XojoScriptMessages
	#tag Method, Flags = &h1
		Protected Function ErrorCodeToString(code as XojoScript.Errors, extraInfo As Dictionary) As String
		  /// Creates a non-localized user-friendly string from an error code.
		  ///
		  /// @param code The error code.
		  /// @param extraInfo The extra information given from XojoScript.
		  /// @result A string describing the error.
		  
		  Static errors As New Dictionary( _
		  XojoScript.Errors.Syntax: "Syntax error.", _
		  XojoScript.Errors.TypeMismatch: "Type mismatch error.  Expected %FirstType, but got %SecondType.", _
		  XojoScript.Errors.ParserStackOverflow: "This line is too complex. Please simplify it.", _
		  XojoScript.Errors.TooManyParameters: "This method requires fewer parameters than were passed.", _
		  XojoScript.Errors.NotEnoughParameters: "This method requires more parameters than were passed.", _
		  XojoScript.Errors.WrongNumberOfParameters: "Wrong number of parameters.", _
		  XojoScript.Errors.IncompatibleParameterList: "Parameters are not compatible with this function.", _
		  XojoScript.Errors.IncompatibleParameterAssignment: "Type mismatch error. A %FirstType parameter can't have a %SecondType default value.", _
		  XojoScript.Errors.UndefinedIdentifier: "This item does not exist.", _
		  XojoScript.Errors.UndefinedOperator: "You have used an operator that is not compatible with the data types specified.", _
		  XojoScript.Errors.LogicOpsRequireBooleans: "Logic operations require boolean values.", _
		  XojoScript.Errors.NonIntArrayBounds: "Array bounds must be integers.", _
		  XojoScript.Errors.CallNonFunction: "Can't call something that isn't a function.", _
		  XojoScript.Errors.SubscriptNonArray: "This is not an array but you are using it as one.", _
		  XojoScript.Errors.NotEnoughDimensions: "This array has more dimensions than you have provided.", _
		  XojoScript.Errors.TooManyDimensions: "This array has fewer dimensions than you have provided.", _
		  XojoScript.Errors.ArrayMethod1DOnly: "This array method works only for one-dimensional arrays.", _
		  XojoScript.Errors.ArrayCast1DOnly: "This type conversion is only implemented for one-dimensional arrays.", _
		  XojoScript.Errors.ExpressionByRef: "You can't pass an expression as a parameter that is defined as ByRef.", _
		  XojoScript.Errors.DuplicateIdentifier: "Redefined identifier. This name is already defined by %Path.", _
		  XojoScript.Errors.BackendFailed: "Code generator failed.", _
		  XojoScript.Errors.AmbiguousMethodCall: "There are several items with this name and it is not clear which one the call refers to.", _
		  XojoScript.Errors.NoMultipleInheritance: "Multiple inheritance is not allowed.", _
		  XojoScript.Errors.ImplementNonInterface: "One of the interfaces of this class is not of type class interface.", _
		  XojoScript.Errors.InheritNonClass: "You can only inherit from a class.", _
		  XojoScript.Errors.DoesNotFullyImplementInterface: "This class is missing the %MethodName method from the interface %InterfaceName.", _
		  XojoScript.Errors.ArrayNegativeSize: "This ubound value is less than -1, but an array cannot have fewer than zero elements.", _
		  XojoScript.Errors.CantIgnoreFunctionResult: "You must use the value returned by this function.", _
		  XojoScript.Errors.CantUseSelfOutsideAClass: """Self"" does not mean anything in a module method.", _
		  XojoScript.Errors.CantUseMeOutsideAClass: """Me"" does not mean anything in a module method.", _
		  XojoScript.Errors.ReturnValueFromSub: "You cannot return a value because this method has not defined a return type.", _
		  XojoScript.Errors.NonExceptionType: "Exception objects must be subclasses of RuntimeException.", _
		  XojoScript.Errors.MismatchReturnInterfaceMethod: "Return type (%FirstType) does not match interface method's return type (%SecondType).", _
		  XojoScript.Errors.MismatchReturnOverrideMethod: "Return type (%FirstType) does not match overridden method's return type (%SecondType).", _
		  XojoScript.Errors.OnlyObjectsHaveEvents: "Event handlers are only legal inside classes.", _
		  XojoScript.Errors.ImplementNonexistentEvent: "You cannot implement a nonexistent event.", _
		  XojoScript.Errors.NoDestructorParameters: "Destructors can't have parameters.", _
		  XojoScript.Errors.CantUseSuperOutsideAClass: "Modules do not have superclasses, so ""Super"" does not mean anything in a module method.", _
		  XojoScript.Errors.CantUseSuperWithoutASuper: "The current class does not have a superclass, so ""Super"" does not mean anything in this method.", _
		  XojoScript.Errors.UnbalancedCompileElse: "This #else does not have a matching #if preceding it.", _
		  XojoScript.Errors.UnbalancedCompileEndIf: "This #endif does not have a matching #if preceding it.", _
		  XojoScript.Errors.CompileIfConditionNotBoolean: "Only Boolean constants can be used with #if.", _
		  XojoScript.Errors.CompileIfConditionNotConstant: "Only Boolean constants can be used with #if.", _
		  XojoScript.Errors.ForLoopMultiplyIndexed: "The Next variable (%TrailingVariable) does not match the loop's counter variable (%LoopVariable).", _
		  XojoScript.Errors.ArrayBoundsMustBeConstant: "The size of an array must be a constant or number.", _
		  XojoScript.Errors.ArrayTypeInExternalCode: "You can't pass an array to an external function.", _
		  XojoScript.Errors.ObjectTypeInExternalCode: "External functions cannot use objects as parameters.", _
		  XojoScript.Errors.StringTypeInExternalCode: "External functions cannot use ordinary strings as parameters. Use CString, PString, WString, or CFStringRef instead.", _
		  XojoScript.Errors.UnsortableDataType: "This kind of array can not be sorted.", _
		  XojoScript.Errors.AccessProtectedProperty: "This property is protected. It can only be used from within its class.", _
		  XojoScript.Errors.AccessProtectedMethod: "This method is protected. It can only be called from within its class.", _
		  XojoScript.Errors.NameDuplicatesDeclareFunction: "This local variable or constant has the same name as a Declare in this method. You must resolve this conflict.", _
		  XojoScript.Errors.NameDuplicatesFunction: "This global variable has the same name as a global function. You must resolve this conflict.", _
		  XojoScript.Errors.NameDuplicatesMethod: "This property has the same name as a method. You must resolve this conflict.", _
		  XojoScript.Errors.NameDuplicatesEvent: "This property has the same name as an event. You must resolve this conflict.", _
		  XojoScript.Errors.NameDuplicatesClass: "This global variable has the same name as a class. You must resolve this conflict.", _
		  XojoScript.Errors.NameDuplicatesModule: "This global variable has the same name as a module. You must change one of them.", _
		  XojoScript.Errors.NameDuplicatesConstant: "This local variable or parameter has the same name as a constant. You must resolve this conflict.", _
		  XojoScript.Errors.NameIsReservedKeyword: "%Identifier is reserved and can't be used as a variable or property name.", _
		  XojoScript.Errors.NoClassByThatName: "There is no class with this name.", _
		  XojoScript.Errors.DeclareLibMustBeStringConstant: "The library name must be a string constant.", _
		  XojoScript.Errors.DeclareFunctionWithNoReturnType: "This Declare Function statement is missing its return type.", _
		  XojoScript.Errors.CantInstantiateAbstractClass: "You can't use the New operator with this class.", _
		  XojoScript.Errors.SubReturnsNoValue: "This method doesn't return a value.", _
		  XojoScript.Errors.MissingEndQuote: "End quote missing.", _
		  XojoScript.Errors.ClassDescendsFromSelf: "A class cannot be its own superclass.", _
		  XojoScript.Errors.AssignToReadOnlyProperty: "Cannot assign a value to this property.", _
		  XojoScript.Errors.ReadFromWriteOnlyProperty: "Cannot get this property's value.", _
		  XojoScript.Errors.IfMissingCondition: "The if statement is missing its condition.", _
		  XojoScript.Errors.MissingReturnTypeInFunction: "The current function must return a value, but this Return statement does not specify any value.", _
		  XojoScript.Errors.IncompatibleParameterOptions: "Parameter options %FirstOption and %SecondOption are incompatible.", _
		  XojoScript.Errors.DuplicateParamOption: "Parameter option %Option was already specified.", _
		  XojoScript.Errors.ByRefWithDefaultValue: "A parameter passed by reference cannot have a default value.", _
		  XojoScript.Errors.ParamArrayWithDefaultValue: "A ParamArray cannot have a default value.", _
		  XojoScript.Errors.AssignsParamWithDefaultValue: "An Assigns parameter cannot have a default value.", _
		  XojoScript.Errors.ExtendsParamWithDefaultValue: "An Extends parameter cannot have a default value.", _
		  XojoScript.Errors.ExtendsParamMustBeFirst: "Only the first parameter may use the Extends option.", _
		  XojoScript.Errors.AssignsParamMustBeLast: "Only the last parameter may use the Assigns option.", _
		  XojoScript.Errors.ParamArrayMustBeLast: "An ordinary parameter cannot follow a ParamArray.", _
		  XojoScript.Errors.OnlyOneAssignsParamPerMethod: "Only one parameter may use the Assigns option.", _
		  XojoScript.Errors.OnlyOneParamArrayPerMethod: "Only one parameter may use the ParamArray option.", _
		  XojoScript.Errors.ParamArray1DOnly: "A ParamArray cannot have more than one dimension.", _
		  XojoScript.Errors.IfWithoutThen: "The keyword 'Then' is expected after this if statement's condition.", _
		  XojoScript.Errors.ConstantWithVariableValue: "Constants must be defined with constant values.", _
		  XojoScript.Errors.IllegalUseOfCall: "Illegal use of the Call keyword.", _
		  XojoScript.Errors.CaseFollowingCaseElse: "No cases may follow the Else block.", _
		  XojoScript.Errors.UnknownPropertyAccessorType: """%AccessorType"" is not a legal property accessor type.", _
		  XojoScript.Errors.MismatchedPropertyAccessorTypes: "This %1 accessor must end with ""End %1"", not ""End %2"".", _
		  XojoScript.Errors.DuplicateMethodDeclaration: "Duplicate method definition.", _
		  XojoScript.Errors.EmptyDeclareLibString: "The library name for this declaration is blank.", _
		  XojoScript.Errors.IfMissingEndIf: "This If statement is missing an End If statement.", _
		  XojoScript.Errors.SelectMissingEndSelect: "This Select Case statement is missing an End Select statement.", _
		  XojoScript.Errors.ForMissingNext: "This For loop is missing its Next statement.", _
		  XojoScript.Errors.WhileMissingWend: "This While loop is missing its Wend statement.", _
		  XojoScript.Errors.TryMissingEndTry: "This Try statement is missing an End Try statement.", _
		  XojoScript.Errors.DoMissingLoop: "This Do loop is missing its Loop statement.", _
		  XojoScript.Errors.TooFewParentheses: "Too few parentheses.", _
		  XojoScript.Errors.TooManyParentheses: "Too many parentheses.", _
		  XojoScript.Errors.CantUseContinueOutsideOfALoop: "The Continue statement only works inside a loop.", _
		  XojoScript.Errors.CouldntFindMatchingLoop: "There is no %BlockType block to %Reason here.", _
		  XojoScript.Errors.CantAccessInstancePropertyFromSharedMethod: "Shared methods cannot access instance properties.", _
		  XojoScript.Errors.CantAccessInstanceMethodFromSharedMethod: "Shared methods cannot access instance properties.", _
		  XojoScript.Errors.CantAccessInstancePropertyFromSharedPropertyAccessor: "Shared computed property accessors cannot access instance properties.", _
		  XojoScript.Errors.CantAccessInstanceMethodFromSharedPropertyAccessor: "Shared computed property accessors cannot access instance methods.", _
		  XojoScript.Errors.ConstructorIsProtected: "The constructor of this class is protected, and can only be called from within this class.", _
		  XojoScript.Errors.StructureStringWithNoLength: "This string field needs to specify its length.", _
		  XojoScript.Errors.StructureCantContainRefType: "Structures cannot contain %FieldType fields.", _
		  XojoScript.Errors.StructureArrays1DOnly: "Structures cannot contain multidimensional arrays.", _
		  XojoScript.Errors.EnumsOnlyInt: "Enumerated types can only contain integers.", _
		  XojoScript.Errors.NoStackedEnums: "An enumeration cannot be defined in terms of another enumeration.", _
		  XojoScript.Errors.AssignToConstant: "This is a constant; its value can't be changed.", _
		  XojoScript.Errors.IllegalStructureStringLength: "A string field must be at least 1 byte long.", _
		  XojoScript.Errors.NonStringFieldWithLength: "The storage size specifier only applies to string fields.", _
		  XojoScript.Errors.StructureContainsSelf: "A structure cannot contain itself.", _
		  XojoScript.Errors.AccessPrivateType: "This type is private, and can only be used within its module.", _
		  XojoScript.Errors.GlobalItemInClass: "Class members cannot be global.", _
		  XojoScript.Errors.ProtectedItemInModule: "Module members must be public or private; they cannot be protected.", _
		  XojoScript.Errors.GlobalItemInInnerModule: "Members of inner modules cannot be global.", _
		  XojoScript.Errors.DimMultipleUsingNewObjectShortcut: "A Dim statement creates only one new object at a time.", _
		  XojoScript.Errors.ConstValueExpected: "A constant was expected here, but this is some other kind of expression.", _
		  XojoScript.Errors.AccessPrivateModule: "This module is private, and can only be used within its containing module.", _
		  XojoScript.Errors.DuplicatePropertyDeclaration: "Duplicate property definition.", _
		  XojoScript.Errors.InvalidArrayElementType: "This datatype cannot be used as an array element.", _
		  XojoScript.Errors.DelegateOptionalParameter: "Delegate parameters cannot be optional.", _
		  XojoScript.Errors.DelegateParameterRole: "Delegates cannot use Extends, Assigns, or ParamArray.", _
		  XojoScript.Errors.RbScriptSandboxDeclare: "The Declare statement is illegal in RbScript.", _
		  XojoScript.Errors.RbScriptSandboxPtr: "It is not legal to dereference a Ptr value in an RbScript.", _
		  XojoScript.Errors.RbScriptSandboxDelegateFromPtr: "Delegate creation from Ptr values is not allowed in RbScript.", _
		  XojoScript.Errors.DuplicateConstantDeclaration: "Duplicate constant definition.", _
		  XojoScript.Errors.AmbiguousInterfaceImplementation: "Ambiguous interface method implementation.", _
		  XojoScript.Errors.ClassDoesNotImplement: "Illegal explicit interface method implementation.  The class does not claim to implement this interface.", _
		  XojoScript.Errors.InterfaceMethodDoesNotExist: "The interface does not declare this method.", _
		  XojoScript.Errors.UnbalancedCompileIf: "This method contains a #if without a closing #endif statement.", _
		  XojoScript.Errors.CyclicalInterfaceAggregate: "This interface contains a cyclical interface aggregation.", _
		  XojoScript.Errors.ExtendsOnClass: "The extends modifier cannot be used on a class method.", _
		  XojoScript.Errors.IncompatibleAssignment: "You cannot assign a non-value type to a value.", _
		  XojoScript.Errors.DuplicateAttribute: "Duplicate attribute name.", _
		  XojoScript.Errors.DelegateReturnStructure: "Delegates cannot return structures.", _
		  XojoScript.Errors.IllegalDelegateDispatch: "You cannot create a delegate from this identifier.", _
		  XojoScript.Errors.IllegalConversionTo: "You cannot use an Operator_Convert method to perform a convert-to operation on an interface.", _
		  XojoScript.Errors.ElseIfMissingCondition: "The ElseIf statement is missing its condition.", _
		  XojoScript.Errors.IllegalConstType: "This type cannot be used as an explicit constant type.", _
		  XojoScript.Errors.RecursiveConstantDeclaration: "Recursive constant declaration error.", _
		  XojoScript.Errors.Custom: "%Message", _
		  XojoScript.Errors.NotALocalVariable: "This is not a local variable or parameter.", _
		  XojoScript.Errors.MaxUlpsMustBeConstant: "The maximum units in last position parameter must be a constant.", _
		  XojoScript.Errors.MaxUlpsOutOfRange: "The maximum units in last position parameter is out of range.", _
		  XojoScript.Errors.StructureFieldAlignment: "The StructureAlignment attribute's value must be one of the following: 1, 2, 4, 8, 16, 32, 64 or 128.", _
		  XojoScript.Errors.RbScriptSandboxPairs: "Pair creation via the : operator is not allowed in RbScript.", _
		  XojoScript.Errors.RbScriptSandboxGetTypeInfo: "Introspection via the GetTypeInfo operator is not allowed in RbScript.", _
		  XojoScript.Errors.ForNextMissingCondition: "The For statement is missing its condition.", _
		  XojoScript.Errors.WhileWendMissingCondition: "The While statement is missing its condition.", _
		  XojoScript.Errors.UnsignedExpressionInNegativeStep: "Unsigned integer used in negative step loops can cause an infinite loop.", _
		  XojoScript.Errors.MustUseObjectsWithIs: "Must use Objects with Is.", _
		  XojoScript.Errors.MustUseObjectsWithAddRemoveHandler: "Only objects can be used with %Keyword.", _
		  XojoScript.Errors.EventDoesNotExist: "%TypeName does not have an event named %EventName.", _
		  XojoScript.Errors.RbScriptSandboxDelegateToPtr: "Converting Delegates to Ptrs is not allowed in RbScript.", _
		  XojoScript.Errors.WeakAddressOfMustHaveInstanceMethod: "WeakAddressOf can only be used on instance methods.", _
		  XojoScript.Errors.RuntimeDeclaresNotAllowed: "Declares directly into the runtime via Lib "" are no longer allowed.", _
		  XojoScript.Errors.ObjCDeclaresMustHaveTarget: "Objective-C declares must have at least one parameter (the target of the message).", _
		  XojoScript.Errors.ShadowedPropertyTypeMismatch: "This property shadows a property of a different type.", _
		  XojoScript.Errors.JumpToMissingLabel: "Goto target not found (%Label).", _
		  XojoScript.Errors.PragmaWarningNeedsMessage: "'#pragma warning' requires an warning message.", _
		  XojoScript.Errors.PragmaErrorNeedsMessage: "'#pragma error' requires an error message." )
		  #If XojoVersion >= 2014
		    // Added in 2014r1
		    errors.Value( XojoScript.Errors.DuplicateLabel ) = "Duplicate label."
		  #EndIf
		  #If XojoVersion > 2014.01
		    // Added in 2014r2
		    errors.Value( XojoScript.Errors.ObjectPropertyWithDefaultValue ) = "Object properties cannot have default values."
		    errors.Value( XojoScript.Errors.ArrayPropertyWithDefaultValue ) = "Array properties cannot have default values."
		  #EndIf
		  #If XojoVersion >= 2014.03
		    // Added in 2014r3
		    errors.Value(XojoScript.Errors.UndefinedMemberIdentifier) = "Type ""%TypeName"" has no member named ""%Name""."
		    errors.Value(XojoScript.Errors.UsingOnlyImportsModules) = "Only modules can be imported, but this is not a module."
		    errors.Value(XojoScript.Errors.CantImportSymbolOverExistingSymbol) = "Can't import ""%TypeName"" because its member ""%Name"" would conflict with the ""%ExistingName"" that is already defined in this block."
		    errors.Value(XojoScript.Errors.AssignToType) = "This is a type name, not a variable; values can't be assigned to it."
		    errors.Value(XojoScript.Errors.AssignToModule) = "This is a module, not a variable; values can't be assigned to it."
		    errors.Value(XojoScript.Errors.UnresolvedType) = "Can't find a type with this name."
		    errors.Value(XojoScript.Errors.TypeRefIsNonTypeSymbol) = "Expected a type name but found %TypeName instead."
		    errors.Value(XojoScript.Errors.UnresolvedNamespace) = "Can't find a type or module with this name."
		    errors.Value(XojoScript.Errors.NamespaceRefIsNonNamespaceSymbol) = "Expected a type or module name here but found %Name instead."
		    errors.Value(XojoScript.Errors.ExtensionMethodRequiresConversions) = "Extension method %Name requires a conversion from %OriginalType to %NewType; use CType to explicitly convert first."
		    errors.Value(XojoScript.Errors.UnresolvedTypeWithSuggestion) = "Can't find a type with this name. Did you mean %Name?"
		    errors.Value(XojoScript.Errors.UndefinedBinop) = "Undefined operator. Type %LeftType does not define ""%Operator"" with type %RightType."
		    errors.Value(XojoScript.Errors.UndefinedMonop) = "Undefined operator. Type %TypeName does not define ""%Operator""."
		    errors.Value(XojoScript.Errors.ImportGlobalMember) = "Cannot import %Name from %Source because it is Global, not Public."
		    errors.Value(XojoScript.Errors.ImportPrivateMember) = "Cannot import %Name from %Source because it is Private to its container, not Public."
		    errors.Value(XojoScript.Errors.NamespaceAsValue) = "Expected a value of type %FirstType, but found a static namespace reference to %SecondType instead."
		    errors.Value(XojoScript.Errors.InstantiateNonClass) = "Cannot create an instance of %TypeName with New because it is not a class."
		    errors.Value(XojoScript.Errors.InstantiateNonClassWithSuggestion) = "Cannot create an instance of %TypeName with New because it is not a class. Did you mean %Replacement?"
		    errors.Value(XojoScript.Errors.TooManyArgs) = "Too many arguments: got %ArgCount, expected only %ParamCount."
		    errors.Value(XojoScript.Errors.TooManyArgsWithOptionals) = "Too many arguments: got %ArgCount, expected no more than %ParamCount."
		    errors.Value(XojoScript.Errors.OneMissingArg) = "Missing argument: need a %TypeName value for parameter %Name."
		    errors.Value(XojoScript.Errors.NotEnoughArgs) = "Not enough arguments: got %ArgCount, expected %ParamCount."
		    errors.Value(XojoScript.Errors.NotEnoughArgsWithOptionals) = "Not enough arguments: got %ArgCount, expected at least %ParamCount."
		    errors.Value(XojoScript.Errors.AssignmentMethodNeedsAssignment) = "Assignment accessor must be invoked by assigning a value."
		    errors.value(XojoScript.Errors.AssignToNonAssignmentMethod) = "This method cannot accept an assigned value (it lacks an Assigns parameter)."
		    errors.value(XojoScript.Errors.OneArgIsIncompatibleWithParam) = "Parameter ""%Name"" expects %FirstType, but this is %SecondType."
		    errors.value(XojoScript.Errors.ArgsAreIncompatibleWithParams) = "Expected (%ParamTypes), but these arguments are (%ArgTypes)."
		    errors.value(XojoScript.Errors.OneArgIsIncompatibleWithParamArray) = "ParamArray ""%Name"" expects values of %FirstType, but this is %SecondType."
		    errors.value(XojoScript.Errors.CallInstanceMethodInSharedContext) = "Instance methods need an object: call this on an instance of %Type."
		    errors.value(XojoScript.Errors.CallExtendsMethodWithNoBaseExp) = "Extension methods need a base expression: call this on a value of %Type."
		    errors.value(XojoScript.Errors.CallInstanceMethodOnNamespace) = "Static reference to instance method: call this on an instance of %Type."
		    errors.value(XojoScript.Errors.CallExtendsMethodOnNamespace) = "Static reference to extension method: call this on a value of %Type."
		    errors.value(XojoScript.Errors.IncompatibleBaseExpression) = "This method extends %FirstType, but the base expression is %SecondType."
		    errors.value(XojoScript.Errors.ProtectedOperatorConvert) = "Cannot convert from %OriginalType to %NewType because %Converter is protected."
		    // FixedLengthStringsAreGone never gets triggered and will likely be removed in the future.
		  #EndIf
		  
		  Dim messageTemplate As String = errors.Lookup(code, "")
		  If messageTemplate <> "" Then
		    Return Substitute(messageTemplate, extraInfo)
		  Else
		    Return "Error " + Str(code)
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function Substitute(template As String, params As Dictionary) As String
		  /// Performs printf style string interpolation, but using dictionary keys instead
		  /// of indices. The key name is specified following a percent sign. Two percent
		  /// signs will result in a single percent sign being emitted.
		  ///
		  /// For example, the following code will yield "hello world":
		  ///   Dim d As New Dictionary( "place": "world" )
		  ///   Dim s As String = Substitute( "hello %place", d )
		  ///
		  /// @param template The template string.
		  /// @param params The parameters to use for replacing placeholders in the template.
		  /// @result The formatted string.
		  
		  Static re As RegEx
		  If re Is Nil Then
		    re = New RegEx
		    re.SearchPattern = "%([a-zA-Z0-9]+|%)"
		  End If
		  
		  Dim parts() As String
		  Dim lastMatchEnd As Integer = 0
		  Dim match As RegExMatch = re.Search(template)
		  While match <> Nil
		    Dim exprStart As Integer = match.SubExpressionStartB(0)
		    Dim exprLength As Integer = match.SubExpressionString(0).LenB
		    parts.Append(template.MidB(lastMatchEnd + 1, exprStart - lastMatchEnd))
		    
		    Dim key As String = match.SubExpressionString(1)
		    If key = "%" Then
		      parts.Append("%")
		    Else
		      parts.Append(params.Lookup(key, ""))
		    End If
		    
		    lastMatchEnd = exprStart + exprLength
		    match = re.Search
		  Wend
		  
		  parts.Append template.MidB(lastMatchEnd + 1, template.LenB - lastMatchend)
		  Return Join(parts, "")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Function WarningCodeToString(code as XojoScript.Warnings, extraInfo As Dictionary) As string
		  /// Creates a user-friendly string from a warning code.
		  ///
		  /// @param code The warning code.
		  /// @param extraInfo The extra information given from XojoScript.
		  /// @result A string describing the warning.
		  
		  Static warnings As New Dictionary( _
		  XojoScript.Warnings.Deprecated: "%Name is deprecated.", _
		  XojoScript.Warnings.DeprecatedWithReplacement: "%Name is deprecated.  You should use %Replacement instead.", _
		  XojoScript.Warnings.OldStyleConstructor: "Old-style constructor methods are no longer supported.  You should use ""Constructor"" instead.", _
		  XojoScript.Warnings.UnknownPragmaWarning: "Unknown pragma name.", _
		  XojoScript.Warnings.ConversionPrecision: "Converting from %OriginalType to %NewType causes a possible loss of precision, which can lead to unexpected results.", _
		  XojoScript.Warnings.ConversionSign: "Converting from %OriginalType to %NewType causes the sign information to be lost, which can lead to unexpected results.", _
		  XojoScript.Warnings.FloatingPointComparison: "Performing a %Operator comparison on floating-point values can yield unexpected results due to their inexact binary representation.", _
		  XojoScript.Warnings.Custom: "%Message", _
		  XojoScript.Warnings.UnusedLocal: "%Name is an unused local variable.", _
		  XojoScript.Warnings.UnusedMethodParameter: "%Name is an unused method parameter.", _
		  XojoScript.Warnings.UnusedEventParameter: "%Name is an unused event parameter.", _
		  XojoScript.Warnings.ShadowedProperty: "This property shadows one already defined by %Superclass.", _
		  XojoScript.Warnings.ShadowedConstant: "This constant shadows one already defined by %Superclass." )
		  #if XojoVersion >= 2014.03
		    // Added in 2014r3
		    warnings.Value(XojoScript.Warnings.NameLookupChange) = _
		    "Before 2014r3, this would have referred to the %OldPath but now it refers to the %NewPath."
		  #endif
		  
		  Dim messageTemplate As String = warnings.Lookup(code, "")
		  If messageTemplate <> "" Then
		    Return Substitute(messageTemplate, extraInfo)
		  Else
		    Return "Warning " + Str(code)
		  End If
		End Function
	#tag EndMethod


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
