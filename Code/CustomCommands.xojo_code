#tag Class
Protected Class CustomCommands
	#tag Method, Flags = &h0
		Sub ClearGraphicsOutput()
		  If GraphicsOutput <> Nil Then
		    GraphicsOutput.ClearGraphics
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ClearTextOutput()
		  If TextOutput <> Nil Then
		    TextOutput.ClearText
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawLine(x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer)
		  If GraphicsOutput <> Nil Then
		    GraphicsOutput.DrawLine(x1, y1, x2, y2)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawOval(x As Integer, y As Integer, w As Integer, h As Integer)
		  If GraphicsOutput <> Nil Then
		    GraphicsOutput.DrawOval(x, y, w, h)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawPicture(x As Integer, y As Integer, picturePath As String)
		  If GraphicsOutput <> Nil Then
		    Dim file As FolderItem = GetFolderItem(picturePath, FolderItem.PathTypeNative)
		    
		    If file <> Nil And file.Exists Then
		      Dim p As Picture = Picture.Open(file)
		      
		      If p <> Nil Then
		        GraphicsOutput.DrawPicture(x, y, p)
		      End If
		    End If
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawRect(x As Integer, y As Integer, w As Integer, h As Integer)
		  If GraphicsOutput <> Nil Then
		    GraphicsOutput.DrawRect(x, y, w, h)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawString(s As String, x As Integer, y As Integer, w As Integer = 0, condense As Boolean = False)
		  If GraphicsOutput <> Nil Then
		    GraphicsOutput.DrawString(s, x, y, w, condense)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function EndOfLine() As String
		  Return Realbasic.EndOfLine
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function EndTimer() As Double
		  Return (Microseconds - StartMicroSeconds) / 1000000
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub FillOval(x As Integer, y As Integer, w As Integer, h As Integer)
		  If GraphicsOutput <> Nil Then
		    GraphicsOutput.FillOval(x, y, w, h)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub FillRect(x As Integer, y As Integer, w As Integer, h As Integer)
		  If GraphicsOutput <> Nil Then
		    GraphicsOutput.FillRect(x, y, w, h)
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ForeColor(Assigns c As Color)
		  If GraphicsOutput <> Nil Then
		    GraphicsOutput.ForeColor = c
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GPIODigitalRead(pin As Integer) As Integer
		  Return GPIO.DigitalRead(pin)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GPIODigitalWrite(pin As Integer, mode As Integer)
		  GPIO.DigitalWrite(pin, mode)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GPIOPinMode(pin As Integer, mode As Integer)
		  GPIO.PinMode(pin, mode)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub GPIOSetupGPIO()
		  GPIO.SetupGPIO
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function GPIOSoftToneCreate(pin As Integer) As Integer
		  Return GPIO.SoftToneCreate(pin)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub GPIOSoftToneWrite(pin As Integer, freq As Integer)
		  GPIO.SoftToneWrite(pin, freq)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GPIO_INPUT() As Integer
		  Return 0
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GPIO_OFF() As Integer
		  Return 0
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GPIO_ON() As Integer
		  Return 1
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GPIO_OUTPUT() As Integer
		  Return 1
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function RandomInt(startValue As Integer, endValue As Integer) As Integer
		  Return App.Randomizer.InRange(startValue, endValue)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SelectFile() As String
		  IsFileSelectorClosed = False
		  
		  // Cannot call UI because the scripts run in a thread so ask
		  // a Timer to show the file selector and then wait for it to be closed.
		  Xojo.Core.Timer.CallLater(300, AddressOf SelectFileTimer)
		  
		  // Wait
		  While IsFileSelectorClosed = False
		  Wend
		  
		  If SelectedFile <> Nil Then
		    Return SelectedFile.NativePath
		  Else
		    Return ""
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SelectFileTimer()
		  SelectedFile = GetOpenFolderItem("")
		  IsFileSelectorClosed = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ShowGraphicsOutput()
		  If GraphicsOutput <> Nil Then
		    IsShowGraphicsOutput = True
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub StartTimer()
		  StartMicroSeconds = Microseconds
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function UserCancelled() As Boolean
		  Return IsUserCancelled
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Wait(ms As Double = 10)
		  App.SleepCurrentThread(ms)
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Yield()
		  App.YieldToNextThread
		  If IsUserCancelled Then
		    
		  End If
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		GraphicsOutput As GraphicsOutputViewer
	#tag EndProperty

	#tag Property, Flags = &h21
		Private IsFileSelectorClosed As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		IsShowGraphicsOutput As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		IsUserCancelled As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private SelectedFile As FolderItem
	#tag EndProperty

	#tag Property, Flags = &h21
		Private StartMicroSeconds As Double
	#tag EndProperty

	#tag Property, Flags = &h0
		TextOutput As TextOutputViewer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsShowGraphicsOutput"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsUserCancelled"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
