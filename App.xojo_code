#tag Class
Protected Class App
Inherits Application
	#tag Event
		Sub Close()
		  SavePreferences
		End Sub
	#tag EndEvent

	#tag Event
		Sub NewDocument()
		  CreateNewWindow
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  Randomizer = New Random
		  
		  LoadPreferences
		  
		  InitRecentItems
		  
		End Sub
	#tag EndEvent


	#tag MenuHandler
		Function FileCloseWindow() As Boolean Handles FileCloseWindow.Action
			// Get the window we want to close and remove it
			CloseThisWindow(Window(0))
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileNew() As Boolean Handles FileNew.Action
			NewDocument
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileOpen() As Boolean Handles FileOpen.Action
			Dim fileInput As New OpenDialog
			Dim f As FolderItem
			f = fileInput.ShowModal
			If f <> Nil Then
			OpenFile(f)
			End If
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function HelpAbout(index as Integer) As Boolean Handles HelpAbout.Action
			AboutWindow.Show
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function HelpOnlineHelp() As Boolean Handles HelpOnlineHelp.Action
			HelpWindow.Show
			
			Return True
			
		End Function
	#tag EndMenuHandler


	#tag Method, Flags = &h0
		Sub AddToWindowMenu(win As ScriptWindow)
		  // If window is already in Window menu, then just update its name
		  Dim count As Integer
		  Dim w As WindowMenuItem
		  
		  count = WindowMenu.Count - 1
		  
		  For i As Integer = 0 To count
		    If WindowMenu.Item(i) IsA WindowMenuItem Then // Ensure the menu is a WindowMenuItem
		      w = WindowMenuItem(WindowMenu.Item(i))
		      
		      // Check if the window referenced in the menu is the window to close
		      If w.IsWindow(win) Then
		        // Update text
		        w.UpdateText
		        Return
		      End If
		    End If
		  Next
		  
		  // Otherwise add it to the Window menu
		  mWindowCounter = mWindowCounter + 1
		  
		  // And add it to the Window menu
		  Dim winMenu As New WindowMenuItem(win)
		  MainMenuBar.Child("WindowMenu").Append(winMenu)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CloseThisWindow(closeWin as Window)
		  // Search for the specified window and close it.
		  Dim count As Integer
		  Dim w As WindowMenuItem
		  
		  count = WindowMenu.Count - 1
		  
		  For i As Integer = 0 To count
		    If WindowMenu.Item(i) IsA WindowMenuItem Then // Ensure the menu is a WindowMenuItem
		      w = WindowMenuItem(WindowMenu.Item(i))
		      
		      // Check if the window referenced in the menu is the window to close
		      If w.IsWindow(closeWin) Then
		        // Remove the MenuItem from Recent Items
		        WindowMenu.Remove(i)
		        
		        // Close the Window
		        closeWin.Close
		        Exit
		      End If
		    End If
		  Next
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub CreateNewWindow(sw As ScriptWindow = Nil)
		  If sw Is Nil Then
		    sw = New ScriptWindow
		    sw.Title = "Xojo Dojo " + Str(mWindowCounter)
		  End If
		  
		  mWindowCounter = mWindowCounter + 1
		  
		  // And add it to the Window menu
		  AddToWindowMenu(sw)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub InitRecentItems()
		  Dim recents() As Auto
		  If Prefs.HasKey("RecentItems") Then
		    recents = Prefs.Value("RecentItems")
		    
		    For i As Integer = 0 To recents.Ubound
		      Dim saveInfo As String = DecodeBase64(recents(i))
		      Dim file As New FolderItem
		      file = file.GetRelative(saveInfo)
		      
		      Dim recentItem As New OpenRecentMenuItem(file.NativePath, saveInfo)
		      
		      FileOpenRecent.Append(recentItem)
		    Next
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub LoadPreferences()
		  Dim prefFolder As FolderItem = SpecialFolder.ApplicationData.Child("Xojo Dojo")
		  If Not prefFolder.Exists Then
		    prefFolder.CreateAsFolder
		  End If
		  
		  Dim prefFileClassic As FolderItem = prefFolder.Child("Preferences.json")
		  PrefsFile = New Xojo.IO.FolderItem(prefFileClassic.NativePath.ToText)
		  If PrefsFile.Exists Then
		    // Load preferences file if it exists
		    Dim prefText As Text
		    Dim prefStream As Xojo.IO.TextInputStream
		    prefStream = Xojo.IO.TextInputStream.Open(PrefsFile, Xojo.Core.TextEncoding.UTF8)
		    prefText = prefStream.ReadAll
		    prefStream.Close
		    
		    Prefs = Xojo.Data.ParseJSON(prefText)
		    
		  Else
		    // Create an initialize new preferences
		    Prefs = New Xojo.Core.Dictionary
		    Prefs.Value("CodeFont") = "Courier"
		    Prefs.Value("CodeFontSize") = 14
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenFile(f As FolderItem)
		  // Open the file in a new window
		  Dim sw As New ScriptWindow
		  sw.OpenFile(f)
		  sw.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SavePreferences()
		  If PrefsFile <> Nil Then
		    // Save up to 10 recent items
		    Dim recents() As String
		    For i As Integer = 0 To Min(FileOpenRecent.Count - 1, 9)
		      Dim b64 As String
		      b64 = EncodeBase64(FileOpenRecent.Item(i).Tag)
		      b64 = DefineEncoding(b64, Encodings.UTF8)
		      recents.Append(b64)
		    Next
		    
		    Prefs.Value("RecentItems") = recents
		    
		    Dim prefsText As Text = Xojo.Data.GenerateJSON(Prefs)
		    Dim prefStream As Xojo.IO.TextOutputStream
		    prefStream = Xojo.IO.TextOutputStream.Create(PrefsFile, Xojo.Core.TextEncoding.UTF8)
		    prefStream.Write(prefsText)
		    prefStream.Close
		  End If
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h21
		Private mWindowCounter As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		Prefs As Xojo.Core.Dictionary
	#tag EndProperty

	#tag Property, Flags = &h21
		Private PrefsFile As Xojo.IO.FolderItem
	#tag EndProperty

	#tag Property, Flags = &h0
		Randomizer As Random
	#tag EndProperty


	#tag Constant, Name = kEditClear, Type = String, Dynamic = False, Default = \"&Delete", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"&Delete"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"&Delete"
	#tag EndConstant

	#tag Constant, Name = kFileQuit, Type = String, Dynamic = False, Default = \"&Quit", Scope = Public
		#Tag Instance, Platform = Windows, Language = Default, Definition  = \"E&xit"
	#tag EndConstant

	#tag Constant, Name = kFileQuitShortcut, Type = String, Dynamic = False, Default = \"", Scope = Public
		#Tag Instance, Platform = Mac OS, Language = Default, Definition  = \"Cmd+Q"
		#Tag Instance, Platform = Linux, Language = Default, Definition  = \"Ctrl+Q"
	#tag EndConstant


	#tag ViewBehavior
	#tag EndViewBehavior
End Class
#tag EndClass
