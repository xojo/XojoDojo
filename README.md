# XojoDojo

Xojo Dojo is a free and easy way to get started with programming on the Raspberry Pi. With Xojo Dojo you can use the Xojo programming language, along with a few other special commands to make simple Pi programs that can use text, graphics and GPIO.

Made open-source as part of Xojo 2018 Just Code Challenge.

## Documentation

https://docs.xojo.com/UserGuide:Xojo_Dojo

## Binary Downloads (latest version)
* Raspberry Pi: http://cdn.xojo.com/Documentation/XojoDojoPi.zip
* Mac: http://cdn.xojo.com/Documentation/XojoDojoMac.zip
* Windows: http://cdn.xojo.com/Documentation/XojoDojoWindows.zip

## Change Log

# 1.1 (October 30, 2019)
* SelectFile method used by ShowPicture example now works.
* Undo now works.
* Added links to Xojo.com and GitLib page on About window.

# 1.0 (August 28, 2018)
Initial release.