#tag Window
Begin ContainerControl GraphicsOutputViewer
   AcceptFocus     =   False
   AcceptTabs      =   True
   AutoDeactivate  =   True
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   Compatibility   =   ""
   DoubleBuffer    =   False
   Enabled         =   True
   EraseBackground =   True
   HasBackColor    =   False
   Height          =   408
   HelpTag         =   ""
   InitialParent   =   ""
   Left            =   0
   LockBottom      =   True
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   TabIndex        =   0
   TabPanelIndex   =   0
   TabStop         =   True
   Top             =   0
   Transparent     =   True
   UseFocusRing    =   False
   Visible         =   True
   Width           =   602
   Begin Canvas OutputCanvas
      AcceptFocus     =   False
      AcceptTabs      =   False
      AutoDeactivate  =   True
      Backdrop        =   0
      DoubleBuffer    =   False
      Enabled         =   True
      EraseBackground =   True
      Height          =   354
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      Scope           =   0
      TabIndex        =   0
      TabPanelIndex   =   0
      TabStop         =   True
      Top             =   0
      Transparent     =   True
      UseFocusRing    =   True
      Visible         =   True
      Width           =   602
   End
   Begin BevelButton ClearButton
      AcceptFocus     =   True
      AutoDeactivate  =   True
      BackColor       =   &c00000000
      Bevel           =   0
      Bold            =   False
      ButtonType      =   0
      Caption         =   "Clear"
      CaptionAlign    =   3
      CaptionDelta    =   0
      CaptionPlacement=   1
      Enabled         =   True
      HasBackColor    =   False
      HasMenu         =   0
      Height          =   30
      HelpTag         =   ""
      Icon            =   0
      IconAlign       =   0
      IconDX          =   0
      IconDY          =   0
      Index           =   -2147483648
      InitialParent   =   ""
      Italic          =   False
      Left            =   11
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   False
      MenuValue       =   0
      Scope           =   0
      TabIndex        =   1
      TabPanelIndex   =   0
      TabStop         =   True
      TextColor       =   &c00000000
      TextFont        =   "System"
      TextSize        =   0.0
      TextUnit        =   0
      Top             =   366
      Transparent     =   False
      Underline       =   False
      Value           =   False
      Visible         =   True
      Width           =   60
   End
   Begin Timer OutputTimer
      Index           =   -2147483648
      LockedInPosition=   False
      Mode            =   2
      Period          =   10
      Scope           =   0
      TabPanelIndex   =   0
   End
   Begin Timer InitGraphicsTimer
      Index           =   -2147483648
      LockedInPosition=   False
      Mode            =   2
      Period          =   10
      Scope           =   0
      TabPanelIndex   =   0
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Sub Open()
		  InitGraphics
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub ClearGraphics()
		  IsInitGraphics = True
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawLine(x1 As Integer, y1 As Integer, x2 As Integer, y2 As Integer)
		  OutputGraphics.Graphics.DrawLine(x1, y1, x2, y2)
		  IsRefresh = True
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawOval(x As Integer, y As Integer, w As Integer, h As Integer)
		  OutputGraphics.Graphics.DrawOval(x, y, w, h)
		  IsRefresh = True
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawPicture(x As Integer, y As Integer, p As Picture)
		  OutputGraphics.Graphics.DrawPicture(p, x, y)
		  IsRefresh = True
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawRect(x As Integer, y As Integer, w As Integer, h As Integer)
		  OutputGraphics.Graphics.DrawRect(x, y, w, h)
		  IsRefresh = True
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub DrawString(s As String, x As Integer, y As Integer, w As Integer = 0, condense As Boolean = False)
		  OutputGraphics.Graphics.DrawString(s, x, y, w, condense)
		  IsRefresh = True
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub FillOval(x As Integer, y As Integer, w As Integer, h As Integer)
		  OutputGraphics.Graphics.FillOval(x, y, w, h)
		  IsRefresh = True
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub FillRect(x As Integer, y As Integer, w As Integer, h As Integer)
		  OutputGraphics.Graphics.FillRect(x, y, w, h)
		  IsRefresh = True
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ForeColor(Assigns c As Color)
		  OutputGraphics.Graphics.ForeColor = c
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub InitGraphics()
		  OutputGraphics = BitmapForCaching(1000, 1000)
		  RefreshCanvas
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub RefreshCanvas()
		  OutputCanvas.Invalidate(False)
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		IsClosed As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private IsInitGraphics As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private IsRefresh As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		OutputGraphics As Picture
	#tag EndProperty


#tag EndWindowCode

#tag Events OutputCanvas
	#tag Event
		Sub Paint(g As Graphics, areas() As REALbasic.Rect)
		  g.DrawPicture(OutputGraphics, 0, 0)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ClearButton
	#tag Event
		Sub Action()
		  InitGraphics
		  OutputCanvas.Invalidate(False)
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events OutputTimer
	#tag Event
		Sub Action()
		  If IsRefresh Then
		    IsRefresh = False
		    OutputCanvas.Invalidate(False)
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events InitGraphicsTimer
	#tag Event
		Sub Action()
		  If IsInitGraphics Then
		    // IsInitGraphics = False
		    // InitGraphics
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="DoubleBuffer"
		Visible=true
		Group="Windows Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="InitialParent"
		Group="Position"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Position"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabPanelIndex"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabIndex"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabStop"
		Visible=true
		Group="Position"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AutoDeactivate"
		Visible=true
		Group="Appearance"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Appearance"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="UseFocusRing"
		Visible=true
		Group="Appearance"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptFocus"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="AcceptTabs"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="EraseBackground"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Transparent"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IsClosed"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="OutputGraphics"
		Group="Behavior"
		Type="Picture"
	#tag EndViewProperty
#tag EndViewBehavior
