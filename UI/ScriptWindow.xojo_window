#tag Window
Begin Window ScriptWindow
   BackColor       =   &cFFFFFF00
   Backdrop        =   0
   CloseButton     =   True
   Compatibility   =   ""
   Composite       =   False
   Frame           =   0
   FullScreen      =   False
   FullScreenButton=   False
   HasBackColor    =   False
   Height          =   578
   ImplicitInstance=   False
   LiveResize      =   True
   MacProcID       =   0
   MaxHeight       =   32000
   MaximizeButton  =   True
   MaxWidth        =   32000
   MenuBar         =   191121407
   MenuBarVisible  =   True
   MinHeight       =   300
   MinimizeButton  =   True
   MinWidth        =   400
   Placement       =   0
   Resizeable      =   True
   Title           =   "Xojo Dojo"
   Visible         =   True
   Width           =   748
   Begin MainToolbar ScriptToolbar
      Enabled         =   True
      Index           =   -2147483648
      InitialParent   =   ""
      LockedInPosition=   False
      Scope           =   0
      TabPanelIndex   =   0
      Visible         =   True
   End
   Begin XojoScript ScriptRunner
      Index           =   -2147483648
      LockedInPosition=   False
      Scope           =   0
      Source          =   ""
      State           =   "0"
      TabPanelIndex   =   0
   End
   Begin Thread CodeThread
      Index           =   -2147483648
      LockedInPosition=   False
      Priority        =   5
      Scope           =   0
      StackSize       =   0
      TabPanelIndex   =   0
   End
   Begin Timer UIUpdateTimer
      Index           =   -2147483648
      LockedInPosition=   False
      Mode            =   2
      Period          =   100
      Scope           =   2
      TabPanelIndex   =   0
   End
   Begin Timer InputTimer
      Index           =   -2147483648
      LockedInPosition=   False
      Mode            =   0
      Period          =   10
      Scope           =   2
      TabPanelIndex   =   0
   End
   BeginSegmented SegmentedControl ScriptSelector
      Enabled         =   True
      Height          =   24
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   20
      LockBottom      =   False
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      MacControlStyle =   0
      Scope           =   2
      Segments        =   "Edit\n\nTrue\rText Output\n\nFalse\rGraphics Output\n\nFalse"
      SelectionType   =   0
      TabIndex        =   5
      TabPanelIndex   =   0
      Top             =   6
      Transparent     =   False
      Visible         =   True
      Width           =   403
   End
   Begin PagePanel ScriptPanel
      AutoDeactivate  =   True
      Enabled         =   True
      Height          =   536
      HelpTag         =   ""
      Index           =   -2147483648
      InitialParent   =   ""
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      PanelCount      =   3
      Panels          =   ""
      Scope           =   2
      TabIndex        =   6
      TabPanelIndex   =   0
      Top             =   42
      Transparent     =   False
      Value           =   0
      Visible         =   True
      Width           =   748
      Begin Listbox ErrorListbox
         AutoDeactivate  =   True
         AutoHideScrollbars=   True
         Bold            =   False
         Border          =   True
         ColumnCount     =   3
         ColumnsResizable=   False
         ColumnWidths    =   "10%,10%"
         DataField       =   ""
         DataSource      =   ""
         DefaultRowHeight=   -1
         Enabled         =   True
         EnableDrag      =   False
         EnableDragReorder=   False
         GridLinesHorizontal=   0
         GridLinesVertical=   0
         HasHeading      =   True
         HeadingIndex    =   -1
         Height          =   146
         HelpTag         =   ""
         Hierarchical    =   True
         Index           =   -2147483648
         InitialParent   =   "ScriptPanel"
         InitialValue    =   "Type	Line	Issue"
         Italic          =   False
         Left            =   20
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   False
         RequiresSelection=   False
         Scope           =   0
         ScrollbarHorizontal=   False
         ScrollBarVertical=   True
         SelectionType   =   0
         ShowDropIndicator=   False
         TabIndex        =   0
         TabPanelIndex   =   1
         TabStop         =   True
         TextFont        =   "System"
         TextSize        =   0.0
         TextUnit        =   0
         Top             =   412
         Transparent     =   False
         Underline       =   False
         UseFocusRing    =   True
         Visible         =   False
         Width           =   708
         _ScrollOffset   =   0
         _ScrollWidth    =   -1
      End
      Begin ScrollBar EditVerticalScrollBar
         AcceptFocus     =   True
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   312
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ScriptPanel"
         Left            =   713
         LineStep        =   1
         LiveScroll      =   True
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   False
         LockRight       =   True
         LockTop         =   True
         Maximum         =   100
         Minimum         =   0
         PageStep        =   20
         Scope           =   0
         TabIndex        =   1
         TabPanelIndex   =   1
         TabStop         =   True
         Top             =   62
         Transparent     =   False
         Value           =   0
         Visible         =   True
         Width           =   15
      End
      Begin ScrollBar EditHorizontalScrollBar
         AcceptFocus     =   True
         AutoDeactivate  =   True
         Enabled         =   True
         Height          =   15
         HelpTag         =   ""
         Index           =   -2147483648
         InitialParent   =   "ScriptPanel"
         Left            =   20
         LineStep        =   1
         LiveScroll      =   True
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   False
         Maximum         =   100
         Minimum         =   0
         PageStep        =   20
         Scope           =   0
         TabIndex        =   2
         TabPanelIndex   =   1
         TabStop         =   True
         Top             =   374
         Transparent     =   False
         Value           =   0
         Visible         =   True
         Width           =   693
      End
      Begin CustomEditField ScriptEditor
         AcceptFocus     =   True
         AcceptTabs      =   False
         AutoCloseBrackets=   False
         AutocompleteAppliesStandardCase=   True
         AutoDeactivate  =   True
         AutoIndentNewLines=   True
         BackColor       =   &cFFFFFF00
         Backdrop        =   0
         Border          =   True
         BorderColor     =   &c88888800
         BracketHighlightColor=   &cFFFF0000
         CaretColor      =   &c00000000
         CaretLine       =   0
         CaretPos        =   0
         ClearHighlightedRangesOnTextChange=   True
         DirtyLinesColor =   &cFF999900
         disableReset    =   False
         DisplayDirtyLines=   False
         DisplayInvisibleCharacters=   False
         DisplayLineNumbers=   True
         DisplayRightMarginMarker=   False
         DoubleBuffer    =   False
         EnableAutocomplete=   True
         Enabled         =   True
         EnableLineFoldings=   False
         enableLineFoldingSetting=   False
         EraseBackground =   False
         GutterBackgroundColor=   &cEEEEEE00
         GutterSeparationLineColor=   &c88888800
         GutterWidth     =   0
         Height          =   312
         HelpTag         =   ""
         HighlightBlocksOnMouseOverGutter=   True
         HighlightMatchingBrackets=   False
         HighlightMatchingBracketsMode=   0
         ignoreRepaint   =   False
         IndentPixels    =   16
         IndentVisually  =   True
         Index           =   -2147483648
         InitialParent   =   "ScriptPanel"
         KeepEntireTextIndented=   True
         Left            =   20
         leftMarginOffset=   4
         LineNumbersColor=   &c88888800
         LineNumbersTextFont=   "System"
         LineNumbersTextSize=   9
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         MaxVisibleLines =   0
         ReadOnly        =   False
         RightMarginAtPixel=   0
         RightScrollMargin=   150
         Scope           =   0
         ScrollPosition  =   0
         ScrollPositionX =   0
         selLength       =   0
         selStart        =   0
         SelText         =   ""
         TabIndex        =   3
         TabPanelIndex   =   1
         TabStop         =   True
         TabWidth        =   2
         Text            =   ""
         TextColor       =   &c00000000
         TextFont        =   ""
         TextHeight      =   0
         TextLength      =   0
         TextSelectionColor=   &c0000FF00
         TextSize        =   0
         ThickInsertionPoint=   True
         Top             =   62
         Transparent     =   True
         UseFocusRing    =   False
         Visible         =   True
         Width           =   693
      End
      Begin TextOutputViewer TextOutputView
         AcceptFocus     =   False
         AcceptTabs      =   True
         AutoDeactivate  =   True
         BackColor       =   &cFFFFFF00
         Backdrop        =   0
         DoubleBuffer    =   False
         Enabled         =   True
         EraseBackground =   True
         HasBackColor    =   False
         Height          =   536
         HelpTag         =   ""
         InitialParent   =   "ScriptPanel"
         IsClosed        =   False
         Left            =   0
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         Scope           =   2
         TabIndex        =   0
         TabPanelIndex   =   2
         TabStop         =   True
         Top             =   42
         Transparent     =   True
         UseFocusRing    =   False
         Visible         =   True
         Width           =   748
      End
      Begin GraphicsOutputViewer GraphicsOutputView
         AcceptFocus     =   False
         AcceptTabs      =   True
         AutoDeactivate  =   True
         BackColor       =   &cFFFFFF00
         Backdrop        =   0
         DoubleBuffer    =   False
         Enabled         =   True
         EraseBackground =   True
         HasBackColor    =   False
         Height          =   536
         HelpTag         =   ""
         InitialParent   =   "ScriptPanel"
         IsClosed        =   False
         Left            =   0
         LockBottom      =   True
         LockedInPosition=   False
         LockLeft        =   True
         LockRight       =   True
         LockTop         =   True
         OutputGraphics  =   0
         Scope           =   2
         TabIndex        =   0
         TabPanelIndex   =   3
         TabStop         =   True
         Top             =   42
         Transparent     =   True
         UseFocusRing    =   False
         Visible         =   True
         Width           =   748
      End
   End
End
#tag EndWindow

#tag WindowCode
	#tag Event
		Function CancelClose(appQuitting as Boolean) As Boolean
		  Return SaveChangesPrompt
		End Function
	#tag EndEvent

	#tag Event
		Sub Close()
		  // Close ourself
		  App.CloseThisWindow(Self)
		End Sub
	#tag EndEvent

	#tag Event
		Sub EnableMenuItems()
		  // Set Checked = True for the current window
		  // Only the top-most window calls this event, so
		  // we first uncheck all the MenuItems and then just
		  // check the one that matches this window.
		  
		  Dim count As Integer
		  Dim w As WindowMenuItem
		  
		  count = WindowMenu.Count - 1
		  
		  For i As Integer = 0 To count
		    If WindowMenu.Item(i) IsA WindowMenuItem Then // Ensure the menu is a WindowMenuItem
		      w = WindowMenuItem(WindowMenu.Item(i))
		      w.Checked = False
		      
		      // Check if the window in WindowMenu is this window (Self)
		      If w.IsWindow(Self) Then
		        w.Checked = True
		      End If
		    End If
		  Next
		  
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  ErrorListBoxDisplay(False)
		End Sub
	#tag EndEvent


	#tag MenuHandler
		Function CodeRun() As Boolean Handles CodeRun.Action
			RunScriptThreaded
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileCloseWindow() As Boolean Handles FileCloseWindow.Action
			Self.Close
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileOpen() As Boolean Handles FileOpen.Action
			Dim fileInput As New OpenDialog
			fileInput.Filter = ScriptFileTypes.XojoDojo
			Dim f As FolderItem
			f = fileInput.ShowModal
			If f <> Nil Then
			If ScriptEditor.Text = "" Then
			OpenFile(f)
			Else
			// Open the file in a new window
			Dim sw As New ScriptWindow
			sw.OpenFile(f)
			sw.Show
			End If
			End If
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileSave() As Boolean Handles FileSave.Action
			Save
			
			Return True
			
		End Function
	#tag EndMenuHandler

	#tag MenuHandler
		Function FileSaveAs() As Boolean Handles FileSaveAs.Action
			SaveAs
			
			Return True
			
		End Function
	#tag EndMenuHandler


	#tag Method, Flags = &h21
		Private Sub AddMessage()
		  Dim locationText As String = Str(ScriptError.Location.Line)
		  ErrorListbox.AddRow("", locationText, ScriptError.MessageText)
		  ErrorListbox.RowTag(ErrorListbox.LastIndex) = ScriptError
		  
		  Dim icon As New Picture(16, 16)
		  If ScriptError.IsError Then
		    icon.Graphics.ForeColor = &cFF0000
		    icon.Graphics.FillOval(0, 0, icon.Width, icon.Height)
		  Else
		    icon.Graphics.ForeColor = &cFF7F00
		    icon.Graphics.FillOval(0, 0, icon.Width, icon.Height)
		  End If
		  
		  ErrorListbox.RowPicture(ErrorListbox.LastIndex) = icon
		  
		  ErrorListBoxDisplay(True)
		  
		  ScriptError = Nil
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub AddToRecentMenu(f As FolderItem)
		  // Add to Recent Items
		  
		  Dim recentItem As New OpenRecentMenuItem(f.NativePath, f.GetSaveInfo(Volume(0), FolderItem.SaveInfoDefaultMode))
		  FileOpenRecent.Insert(0, recentItem)
		  
		  // Remove older entry if exists
		  For i As Integer = FileOpenRecent.Count - 1 DownTo 1
		    If FileOpenRecent.Item(i).Text = f.NativePath Then
		      FileOpenRecent.Remove(i)
		      Exit For
		    End If
		  Next
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function AddYieldCommands(script As String) As String
		  Return script
		  
		  // Add Yield command to end-of-loop constructs: next, wend, loop
		  
		  script = script.ReplaceAll("next", "Yield" + EndOfLine + "next")
		  script = script.ReplaceAll("wend", "Yield" + EndOfLine + "wend")
		  script = script.ReplaceAll("loop", "Yield" + EndOfLine + "loop")
		  script = script.ReplaceAll("continue", "Yield" + EndOfLine + "continue")
		  
		  Return script
		End Function
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ApplySyntaxDefinition()
		  // Load Syntax definition
		  Dim def As New HighlightDefinition
		  If def.LoadFromXml(XojoScriptSyntax) Then
		    ScriptEditor.SyntaxDefinition = def
		    ScriptEditor.ReindentText // cleans up indentations, removing any leading blanks from the lines
		    ScriptEditor.ResetUndo
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub ErrorListBoxDisplay(display As Boolean)
		  Const kGap As Integer = 30
		  
		  If display Then
		    ScriptEditor.Height = ErrorListbox.Top - kGap - ScriptEditor.Top
		    EditVerticalScrollBar.Height = ScriptEditor.Height
		    EditHorizontalScrollBar.Top = ScriptEditor.Top + ScriptEditor.Height + 1
		    ErrorListbox.Visible = True
		  Else
		    ScriptEditor.Height = Self.Height - ScriptEditor.Top - kGap
		    EditVerticalScrollBar.Height = ScriptEditor.Height
		    EditHorizontalScrollBar.Top = ScriptEditor.Top + ScriptEditor.Height + 1
		    ErrorListbox.Visible = False
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub OpenFile(f As FolderItem)
		  AddToRecentMenu(f)
		  
		  ScriptFile = f
		  Self.Title = ScriptFile.Name
		  
		  ScriptEditor.SyntaxDefinition = Nil
		  
		  Dim scriptInput As TextInputStream
		  scriptInput = TextInputStream.Open(ScriptFile)
		  Dim script As String = DefineEncoding(scriptInput.ReadAll, Encodings.UTF8)
		  scriptInput.Close
		  
		  ScriptEditor.Text = script
		  
		  ApplySyntaxDefinition
		  
		  Self.ContentsChanged = False
		  
		  App.AddToWindowMenu(Self)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub RunScript()
		  ErrorListBoxDisplay(False)
		  
		  ErrorListbox.DeleteAllRows
		  ScriptRunner.Source = ScriptEditor.Text
		  
		  Dim commands As New CustomCommands
		  commands.TextOutput = TextOutputView
		  commands.GraphicsOutput = GraphicsOutputView
		  ScriptRunner.Context = commands
		  
		  If ScriptRunner.Precompile(XojoScript.OptimizationLevels.None) Then
		    ScriptRunner.Run
		  Else
		    Beep
		    Return
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub RunScriptThreaded()
		  If CodeThread.State <> Thread.NotRunning Then
		    MsgBox("Your script is already running.")
		    Return
		  End If
		  
		  ErrorListbox.DeleteAllRows
		  ErrorListBoxDisplay(False)
		  
		  GraphicsOutputView.InitGraphics
		  
		  CodeThread.Run
		  
		  If IsShowGraphicsOutput Then
		    GraphicsOutputView.RefreshCanvas
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub Save()
		  If ScriptFile <> Nil Then
		    Dim output As TextOutputStream
		    output = TextOutputStream.Create(ScriptFile)
		    output.Write(ScriptEditor.Text)
		    output.Close
		    Self.ContentsChanged = False
		    App.AddToWindowMenu(Self)
		  Else
		    SaveAs
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub SaveAs()
		  Dim saveFile As FolderItem
		  Dim saveDialog As New SaveAsDialog
		  saveDialog.Filter = ScriptFileTypes.XojoDojo
		  saveFile = saveDialog.ShowModal
		  If saveFile <> Nil Then
		    ScriptFile = saveFile
		    Self.Title = ScriptFile.Name
		    Save
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function SaveChangesPrompt() As Boolean
		  If Self.ContentsChanged Then
		    Dim saveName As String = "Untitled"
		    If ScriptFile <> Nil Then saveName = ScriptFile.Name
		    
		    Dim msg As New MessageDialog
		    msg.Message = "Do you want to save changes to before """ + saveName + """ before closing?"
		    msg.Explanation = "If you don't save, your changes will be lost."
		    msg.ActionButton.Caption = "Save"
		    msg.CancelButton.Visible = True
		    msg.AlternateActionButton.Caption = "Don't Save"
		    msg.AlternateActionButton.Visible = True
		    Dim btn As MessageDialogButton
		    btn = msg.ShowModalWithin(Self)
		    Select Case btn
		    Case msg.ActionButton
		      Save
		      Return False
		    Case Msg.CancelButton
		      Return True
		    Case msg.AlternateActionButton
		      Return False
		      
		    End Select
		  End If
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private InputPrompt As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private InputValue As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private IsInputFinished As Boolean
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  Dim commands As CustomCommands = CustomCommands(ScriptRunner.Context)
			  If commands <> Nil Then
			    Return commands.IsShowGraphicsOutput
			  Else
			    Return False
			  End If
			End Get
		#tag EndGetter
		IsShowGraphicsOutput As Boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private IsShowTextOutput As Boolean
	#tag EndProperty

	#tag Property, Flags = &h21
		Private MyAutocomplete As Patrie
	#tag EndProperty

	#tag Property, Flags = &h21
		Private ScriptError As CompilerMessage
	#tag EndProperty

	#tag Property, Flags = &h21
		Private ScriptFile As FolderItem
	#tag EndProperty


	#tag Constant, Name = kAutocompleteWords, Type = String, Dynamic = False, Default = \"And\nArray\nAs\nAssigns\nByRef\nByVal\nCall\nCase\nConst\nContinue\nCType\nDim\nDo\nDownTo\nEach\nElse\nElseIf\nEnd\nEnum\nExit\nExtends\nFalse\nFor\nFunction\nGlobal\nGoTo\nIf\nIn\nInterface\nIs\nIsA\nLoop\nMod\nModule\nNew\nNext\nNil\nNot\nObject\nOf\nOptional\nOr\nParamArray\nPrivate\nProperty\nProtected\nPublic\nRaiseEvent\nReDim\nRem\nReturn\nSelect\nSelf\nStatic\nStep\nSub\nSuper\nThen\nTo\nTrue\nUntil\nWend\nWhile\nXor\nBoolean\nCurrency\nDouble\nInteger\nSingle\nString\nText\nClearGraphicsOutput\nClearTextOutput\nDrawLine\nDrawOval\nDrawPicture\nDrawRect\nDrawString\nEndOfLine\nEndTimer\nFillOval\nFillRect\nForeColor\nGPIODigitalWrite\nGPIODigitalRead\nGPIOPinMode\nGPIOSetupGPIO\nGPIOSoftToneCreate\nGPIOSoftToneWrite\nGPIO_OFF\nGPIO_ON\nGPIO_OUTPUT\nGPIO_INPUT\nRandomInt\nShowGraphicsOutput\nStartTimer\nUserCancelled\nWait\nYield", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kEditView, Type = Double, Dynamic = False, Default = \"0", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kGraphicsView, Type = Double, Dynamic = False, Default = \"2", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kTextView, Type = Double, Dynamic = False, Default = \"1", Scope = Private
	#tag EndConstant


#tag EndWindowCode

#tag Events ScriptToolbar
	#tag Event
		Sub Action(item As ToolItem)
		  Select Case item.Name
		  Case "RunButton"
		    RunScriptThreaded
		  Case "StopButton"
		    If CodeThread.State <> Thread.NotRunning Then
		      Dim commands As CustomCommands = CustomCommands(ScriptRunner.Context)
		      If commands <> Nil Then
		        commands.IsUserCancelled = True
		      End If
		      // Killing the thread seems to hang on Pi
		      'CodeThread.Kill
		    End If
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ScriptRunner
	#tag Event
		Sub RuntimeError(error As RuntimeException)
		  If error IsA ThreadEndException Then Return
		  
		  Dim dialog As New MessageDialog
		  dialog.Title = "Unhandled Exception"
		  dialog.Message = "Unhandled Exception"
		  dialog.Explanation = "A " + Introspection.GetType(error).Name + " was not caught."
		  If error.Message <> "" Then
		    dialog.Explanation = dialog.Explanation + " " + error.Message
		  End If
		  
		  Call dialog.ShowModalWithin(Self)
		End Sub
	#tag EndEvent
	#tag Event
		Sub CompilerWarning(location As XojoScriptLocation, warning As XojoScript.Warnings, warningInfo As Dictionary)
		  // Only show warnings if that setting is turned on
		  If False Then
		    'AddMessage(False, XojoScriptMessages.WarningCodeToString(warning, warningInfo), location, warningInfo)
		  End If
		End Sub
	#tag EndEvent
	#tag Event
		Function CompilerError(location As XojoScriptLocation, error As XojoScript.Errors, errorInfo As Dictionary) As Boolean
		  IsShowTextOutput = False
		  
		  ScriptError = New CompilerMessage
		  ScriptError.IsError = True
		  ScriptError.Location = location
		  ScriptError.ExtraInfo = errorInfo
		  ScriptError.MessageText = XojoScriptMessages.ErrorCodeToString(error, errorInfo)
		End Function
	#tag EndEvent
	#tag Event
		Sub Print(msg As String)
		  TextOutputView.AddText(msg)
		  
		  IsShowTextOutput = True
		  
		End Sub
	#tag EndEvent
	#tag Event
		Function Input(prompt As String) As String
		  // This code is for non-threaded XojoScript:
		  // Dim inputWin As New InputWindow
		  // Return inputWin.Present(Self, prompt)
		  
		  // When running in a thread, this even needs to:
		  // 1) Use a Timer to tell UI to open InputWindow
		  // 2) Wait until InputWindow gets a result
		  // 3) Return the result
		  
		  IsInputFinished = False
		  InputValue = ""
		  InputPrompt = prompt
		  InputTimer.Mode = Timer.ModeSingle
		  InputTimer.Enabled = True
		  
		  While Not IsInputFinished
		    App.SleepCurrentThread(5)
		  Wend
		  
		  Return InputValue
		End Function
	#tag EndEvent
#tag EndEvents
#tag Events CodeThread
	#tag Event
		Sub Run()
		  ScriptRunner.Source = AddYieldCommands(ScriptEditor.Text)
		  
		  Dim commands As New CustomCommands
		  commands.TextOutput = TextOutputView
		  commands.GraphicsOutput = GraphicsOutputView
		  ScriptRunner.Context = commands
		  
		  If ScriptRunner.Precompile(XojoScript.OptimizationLevels.None) Then
		    ScriptRunner.Run
		  Else
		    Beep
		    Return
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events UIUpdateTimer
	#tag Event
		Sub Action()
		  If IsShowTextOutput Then
		    ScriptSelector.Items(kTextView).Selected = True
		    ScriptPanel.Value = kTextView
		  End If
		  
		  Dim commands As CustomCommands = CustomCommands(ScriptRunner.Context)
		  If commands <> Nil Then
		    If commands.IsShowGraphicsOutput Then
		      ScriptSelector.Items(kGraphicsView).Selected = True
		      ScriptPanel.Value = kGraphicsView
		    End If
		  End If
		  
		  // Display script error message
		  If ScriptError <> Nil Then
		    AddMessage
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events InputTimer
	#tag Event
		Sub Action()
		  IsInputFinished = False
		  Dim inputWin As New InputWindow
		  InputValue = inputWin.Present(Self, InputPrompt)
		  IsInputFinished = True
		  Me.Mode = Timer.ModeOff
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ScriptSelector
	#tag Event
		Sub Action(itemIndex as integer)
		  Select Case itemIndex
		  Case kEditView
		    IsShowTextOutput = False
		    
		    Dim commands As CustomCommands = CustomCommands(ScriptRunner.Context)
		    If commands <> Nil Then
		      commands.IsShowGraphicsOutput = False
		    End If
		    
		    ScriptPanel.Value = kEditView
		  Case kTextView
		    ScriptPanel.Value = kTextView
		  Case kGraphicsView
		    ScriptPanel.Value = kGraphicsView
		  End Select
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ErrorListbox
	#tag Event
		Sub Change()
		  // Highlight the line for the selected error
		  
		  If Me.ListIndex = -1 Then Exit Sub
		  
		  Dim message As CompilerMessage = Me.RowTag(Me.ListIndex)
		  ScriptEditor.SelStart = message.Location.Character
		  ScriptEditor.SelLength = message.Location.EndCharacter - message.Location.Character
		End Sub
	#tag EndEvent
	#tag Event
		Sub ExpandRow(row As Integer)
		  Dim message As CompilerMessage = Me.RowTag(row)
		  For Each key As variant In message.ExtraInfo.keys
		    Me.AddRow(key + ": " + message.ExtraInfo.Value(key))
		    Me.RowTag(Me.LastIndex) = message
		  Next
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events EditVerticalScrollBar
	#tag Event
		Sub ValueChanged()
		  ScriptEditor.ScrollPosition = Me.Value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events EditHorizontalScrollBar
	#tag Event
		Sub ValueChanged()
		  ScriptEditor.ScrollPositionX = Me.Value
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ScriptEditor
	#tag Event
		Sub Open()
		  Me.SetScrollbars(EditHorizontalScrollBar, EditVerticalScrollBar)
		  Me.TextFont = App.Prefs.Value("CodeFont")
		  Me.TextSize = App.Prefs.Value("CodeFontSize")
		  Me.EnableAutocomplete = True
		  
		  MyAutocomplete = New PaTrie
		  Dim words() As String = kAutocompleteWords.Split(EndOfLine)
		  
		  For Each w As String In words
		    Call MyAutocomplete.addKey(w)
		  Next
		  
		  ApplySyntaxDefinition
		  
		  Me.SetFocus
		End Sub
	#tag EndEvent
	#tag Event
		Sub TextChanged()
		  Self.ContentsChanged = Me.IsDirty
		End Sub
	#tag EndEvent
	#tag Event
		Function AutocompleteOptionsForPrefix(prefix as string) As AutocompleteOptions
		  //you can replace this with your own Autocomplete engine...
		  Dim options As New AutocompleteOptions
		  Dim commonPrefix As String
		  
		  //prefix is the word that triggered these Autocomplete options
		  options.Prefix = prefix
		  
		  //options is the array that holds the different Autocomplete options for this word (prefix)
		  options.Options = MyAutocomplete.wordsForPrefix(prefix, commonPrefix)
		  
		  //the longest common prefix of the different options
		  options.LongestCommonPrefix = commonPrefix
		  
		  Return options
		End Function
	#tag EndEvent
	#tag Event
		Function ShouldTriggerAutocomplete(Key as string, hasAutocompleteOptions as boolean) As boolean
		  Return key = Chr(9)
		End Function
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Interfaces"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
		EditorType="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Size"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Size"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Size"
		InitialValue="64"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxWidth"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaxHeight"
		Visible=true
		Group="Size"
		InitialValue="32000"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Frame"
		Visible=true
		Group="Frame"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Document"
			"1 - Movable Modal"
			"2 - Modal Dialog"
			"3 - Floating Window"
			"4 - Plain Box"
			"5 - Shadowed Box"
			"6 - Rounded Window"
			"7 - Global Floating Window"
			"8 - Sheet Window"
			"9 - Metal Window"
			"11 - Modeless Dialog"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Frame"
		InitialValue="Untitled"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="CloseButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Resizeable"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MaximizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinimizeButton"
		Visible=true
		Group="Frame"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreenButton"
		Visible=true
		Group="Frame"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Composite"
		Group="OS X (Carbon)"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MacProcID"
		Group="OS X (Carbon)"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ImplicitInstance"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Placement"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Default"
			"1 - Parent Window"
			"2 - Main Screen"
			"3 - Parent Window Screen"
			"4 - Stagger"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LiveResize"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="FullScreen"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HasBackColor"
		Visible=true
		Group="Background"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="BackColor"
		Visible=true
		Group="Background"
		InitialValue="&hFFFFFF"
		Type="Color"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Backdrop"
		Visible=true
		Group="Background"
		Type="Picture"
		EditorType="Picture"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBar"
		Visible=true
		Group="Menus"
		Type="MenuBar"
		EditorType="MenuBar"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MenuBarVisible"
		Visible=true
		Group="Deprecated"
		InitialValue="True"
		Type="Boolean"
		EditorType="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IsShowGraphicsOutput"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
#tag EndViewBehavior
